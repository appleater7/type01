var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person(name, age) {
        this.name = name;
        this.age = age;
        this.addr = '서울';
    }
    Person.prototype.printInfo = function () {
        console.log('이름 : ' + this.name);
        console.log('나이 : ' + this.age);
    };
    return Person;
}());
var Student = /** @class */ (function (_super) {
    __extends(Student, _super);
    function Student(classNum) {
        var _this = _super.call(this, '강영준', 100) || this;
        _this.classNum = classNum;
        return _this;
    }
    Student.prototype.printInfo = function () {
        _super.prototype.printInfo.call(this);
        console.log(this.classNum);
        console.log('주소 : ' + this.addr);
    };
    return Student;
}(Person));
var School = /** @class */ (function () {
    function School() {
    }
    School.prototype.setPerson = function (p) {
        this.p = p;
    };
    School.prototype.getPerson = function () {
        return this.p;
    };
    School.prototype.setNum = function (num) {
        console.log(num);
    };
    return School;
}());
var persons = [];
for (var i = 0; i < 10; i++) {
    persons.push(new Person('이름' + i, i));
}
for (var _i = 0, persons_1 = persons; _i < persons_1.length; _i++) {
    var p = persons_1[_i];
    p.printInfo();
}
