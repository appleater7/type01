class Person {
    protected addr:string='서울';
    constructor(private name:string, private age:number) {
        
    }
    printInfo():void{
        console.log('이름 : ' + this.name);
        console.log('나이 : ' + this.age);
    }
}
class Student extends Person{
    constructor(private classNum:number) {
        super('강영준',100);
    }
    printInfo():void {
        super.printInfo();
        console.log(this.classNum);
        console.log('주소 : ' + this.addr);
    }
}
class School {
    private p:Person;
    setPerson(p:Person) {
        this.p = p;
    }
    getPerson():Person {
        return this.p;
    }
    setNum(num?:number) {
        console.log(num);
    }
}
var persons:Person[] = [];
for (var i=0;i<10;i++){
    persons.push(new Person('이름'+i,i));
}
for (var p of persons){
    p.printInfo();
}